<?php require('includes/config.php');


$stmt = $db->prepare('SELECT postID, diplomaTheme, diplomaDesc, dipSubDate FROM blog_posts_seo WHERE userSlug = :userSlug');
$stmt->execute(array(':userSlug' => $_GET['id']));
$row = $stmt->fetch();
//if post does not exists redirect user.
if($row['postID'] == ''){
	header('Location: ./');
	exit;
}
?>
<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head -->
	<meta name="keywords" content="keywords" />
	<meta name="description" content="description" />
	<meta name="author" content="">
	<title>Rand</title>
	<!-- Свой CSS -->
	<link rel="stylesheet" href="style/normalize.css">
	<link rel="stylesheet" href="style/main.css">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<!-- JQuery Latest  -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js "></script>
	<!-- JQuery подключать раньше чем bootstrap.js-->
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<!-- Font Awesome + Google Icons -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>
<body role="document">
	<div class="container-fluid h1">Diploma Description
		<a href="./"><div class="btn btn-success">Blog Index</div></a>
	</div>
	<hr />

	<div id="wrap">
		<div class="container">



			<?php $stmt = $db->query('SELECT postID, firstName, middleName, lastName, diplomaTheme, dipSubDate,userSlug, lead_by_teacher FROM blog_posts_seo   '); ?>
			<div class="row">
				<div class="col-md-9">
					<div class="row">
						<div class="col-md-12 ">

							<div class="well well-sm">
								<div class="media">
									<div class="media-left media-middle">
									<?php echo '<img class="img-circle" src="'.$row["student_avatar"].'" alt="" width="75" height="75"> '?>
									</div>
									<div class="media-body text-center">
										<h1 class="media-heading">
											<?php echo $row['diplomaTheme'] ?>
										</h1>
										<br />Author:
										<?php echo $row['firstName'].' '.$row['middleName'].' '.$row['lastName'] ?>
										<br />Registered Diploma on:
										<?php echo date('jS M Y', strtotime($row['dipSubDate'])) ?>
										<br />Lead By Teacher:
										<?php echo $row['lead_by_teacher'] ?>


									</div>
								</div>
							</div>

						</div>


						<?php echo 	$row['diplomaDesc'] ?>

					</div>
				</div>
				<div class="col-md-3">
					<?php require('sidebar.php'); ?>
				</div>
			</div>




			<?php require_once('footer.php'); ?>

		</body>
		</html>