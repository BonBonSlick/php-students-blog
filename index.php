<?php require_once('includes/config.php'); ?>
<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head -->
	<meta name="keywords" content="keywords" />
	<meta name="description" content="description" />
	<meta name="author" content="">
	<title>Rand</title>
	<!-- Свой CSS -->
	<link rel="stylesheet" href="style/normalize.css">
	<link rel="stylesheet" href="style/main.css">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<!-- JQuery Latest  -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js "></script>
	<!-- JQuery подключать раньше чем bootstrap.js-->
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<!-- Font Awesome + Google Icons -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>
<body role="document">

	<!-- NAVIGATION BAR -->
	<nav id="navigation_control" class="navbar navbar-default">
		<div  class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#site-navbar" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/"><i class="fa fa-globe"> Diploma Register </i></a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="site-navbar">
			<ul id="nav_controls" class="nav navbar-nav">
				<li>
					<form  class="navbar-form form-horizontal navbar-left "  role="search" action="search_results.php" method="get">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon2">
									<i class="fa fa-search " aria-hidden="true"></i>
								</span>
								<input type="text" name="enteredterm"  class="form-control" placeholder="Recipient's username" aria-describedby="basic-addon2">
								<span class="input-group-addon" id="basic-addon2">

									<button type="submit" name="search" class="btn btn-primary btn-block">
										Access
									</button>
								</span>




							</div>
						</div>
					</form>
				</li>
			</ul>
		</div><!-- NAV BAR COLLAPSE -->
	</nav>
	<!-- END NAVIGATION BAR -->


	<div id="wrap">
		<div class="container">

			<div class="col-md-9">
				<?php
				try {
					$pages = new Paginator('2','p');
					$stmt = $db->query('SELECT postID FROM blog_posts_seo');
					$pages->set_total($stmt->rowCount());
					$stmt = $db->query('SELECT postID,firstName,middleName,lastName,dipSubDate,diplomaTheme,diploma_desc_short, userSlug, student_avatar, lead_by_teacher FROM blog_posts_seo ORDER BY postID DESC '.$pages->get_limit());
					while($row = $stmt->fetch()){

						echo '
						<div class="well well-sm">
							<div class="media">
								<div class="media-left media-middle">
									<img class="img-circle" src="'.$row["student_avatar"].'" alt="" width="75" height="75">
								</div>
								<div class="media-body text-center">
									<h1 class="media-heading"><a href="'.$row['userSlug'].'"> '.$row['diplomaTheme'].'</a></h1><hr />

									';
									echo '<br />Author: '.$row['firstName'].' '.$row['middleName'].' '.$row['lastName'].
									'<br />Registered Diploma on: '.date('jS M Y', strtotime($row['dipSubDate'])).
									'<br />Lead By Teacher: '.$row['lead_by_teacher']
									;

									echo '</p> ';
									echo  $row['diploma_desc_short'] ;
									echo ' <div class="col-md-12 text-center "><a href="'.$row['userSlug'].'"><div class="btn btn-info text_black">Find Out More...</div></a></div>
								</div>
							</div>
						</div>
						';
					}
					echo $pages->page_links();

				} catch(PDOException $e) {
					echo $e->getMessage();
				}
				?>
			</div>
			<div class="col-md-3">
				<?php require_once('sidebar.php'); ?>
			</div>



			<?php require_once('footer.php'); ?>
		</body>
		</html>