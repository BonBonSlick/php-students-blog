<?php
//include config
require_once('../includes/config.php');

//if not logged in redirect to login page
if(!$user->is_logged_in()){ header('Location: login.php'); }

//show message from add / edit page
if(isset($_GET['delpost'])){

	$stmt = $db->prepare('DELETE FROM blog_posts_seo WHERE postID = :postID') ;
	$stmt->execute(array(':postID' => $_GET['delpost']));

	header('Location: index.php?action=deleted');
	exit;
}

?>

<?php include('menu.php');?>


<div class="container">


	<?php
	//show message from add / edit page
	if(isset($_GET['action'])){
		echo '<div class="container-fluid text-center h1">Student '.$_GET['action'].'</div>';
	}

	?>

	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row">
				<div class="col-md-2"><a href='add-post.php'>
					<div class="btn btn-success"><i class="fa  fa-book"></i> Add Diploma</div>
				</a></div>
				<div class="col-md-10">
					<form  role="search" action="admin_search_results.php" method="get">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon2"><i class="fa fa-blind " aria-hidden="true"></i></span>
								<input type="text"  name="enteredterm" class="form-control" placeholder="Search">
								<span  class="input-group-addon" id="basic-addon2"><i class="fa fa-search " aria-hidden="true"></i>
									<input type="submit" name="search">
								</span>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<?php
		if(isset($_GET['search'])){
			$enteredterm = $_GET['enteredterm'];

			if ($enteredterm ===""){
				echo "Error, enter something.";
			} else {

				$stmt = $db->prepare("SELECT * FROM blog_posts_seo WHERE diplomaTheme LIKE :enteredterm");
				$stmt->bindValue(':enteredterm','%'.$enteredterm.'%');
				$stmt->execute();
				$stmt->execute();
  // loop displays loop
				while ($row = $stmt->fetch(PDO::FETCH_OBJ))
				{
					;?>


					<a href="http://students/<?php echo $row->userSlug;?>" target="_blank">
					 <?php echo $row->postID." ".$row->diplomaTheme;?></a>*




					<?php }
				}
			}
			?>

			<div class="table-responsive">
				<table class="table table-bordered  table-hover">
					<thead class="table-sm">
						<tr class="info">
							<th >#ID</th>
							<th>First Name</th>
							<th>Middle Name</th>
							<th>Last Name</th>
							<th>Lead By Teacher</th>
							<th>Diploma Theme</th>
							<th>Diploma Submission Date</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
						try {

							$stmt = $db->query('SELECT postID, firstName, middleName, lastName, diplomaTheme, dipSubDate,userSlug, lead_by_teacher FROM blog_posts_seo ORDER BY postID DESC');
							while($row = $stmt->fetch()){

								echo '<tr>';
								echo '<th class="info">'.$row['postID'].'</th>';
								echo '<td>'.$row['firstName'].'</td>';
								echo '<td>'.$row['middleName'].'</td>';
								echo '<td>'.$row['lastName'].'</td>';
								echo '<td>'.$row['lead_by_teacher'].'</td>';
								echo '<td>'.$row['diplomaTheme'].'</td>';
								echo '<td>'.date('jS M Y', strtotime($row['dipSubDate'])).'</td>';
								?>

								<td class="info text-center">

									<div class="btn-group ">
										<a href="edit-post.php?id=<?php echo $row['postID'];?>">
											<button class="btn btn-warning btn-responsive text_black" title="View">
												<i class="fa fa-pencil"></i><span class="hidden-xs"> Edit</span>
											</button>
										</a>
										<a href="http://students/<?php echo $row['userSlug'];?>" target="_blank">
											<button class="btn btn-info btn-responsive text_black" title="Delete">
												<i class="fa fa-eye"></i><span class="hidden-xs"> Show</span>
											</button>
										</a>
										<a href="javascript:delpost('<?php echo $row['postID'];?>','<?php echo $row['postTitle'];?>')">
											<button class="btn btn-danger btn-responsive text_black" title="Delete">
												<i class="fa fa-times"></i><span class="hidden-xs"> Delete</span>
											</button>
										</a>


									</div>

								</td>

								<?php
								echo '</tr>';

							}

						} catch(PDOException $e) {
							echo $e->getMessage();
						}
						?>
					</tbody>
				</table>
			</div>
		</div>




	</div>
	<script language="JavaScript" type="text/javascript">
		function delpost(id, title)
		{
			if (confirm("Are you sure you want to delete '" + title + "'"))
			{
				window.location.href = 'index.php?delpost=' + id;
			}
		}
	</script>
</body>
</html>
