<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head -->
	<meta name="keywords" content="keywords" />
	<meta name="description" content="description" />
	<meta name="author" content="">
	<meta name="_token" content="{!! csrf_token() !!}"/>
	<title>Rand</title>
	<!-- Свой CSS -->
	<link rel="stylesheet" href="../style/normalize.css">
	<link rel="stylesheet" href="../style/main.css">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<!-- JQuery Latest  -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js "></script>
	<!-- JQuery подключать раньше чем bootstrap.js-->
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<!-- Font Awesome + Google Icons -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>
<body role="document">

	<div class="container-fluid">
		<div  class="row">

			<div  class="btn-group btn-group-justified " role="group" aria-label="...">
				<div class="btn-group " role="group">
					<a href='index.php' ><button type="button" class="btn btn-info admin_menu_btn">Students List</button></a>
				</div>
				<div class="btn-group " role="group">
					<a href='users.php' role="button"><button type="button" class="btn btn-danger admin_menu_btn">Admins</button></a>
				</div>
				<div  class="btn-group " role="group">
					<a href="../" target="_blank" role="button"><button type="button" class="btn btn-warning admin_menu_btn">View Website</button></a>
				</div>
				<div class="btn-group " role="group">
					<a href='logout.php' role="button"><button type="button" class="btn btn-success admin_menu_btn">Logout</button></a>
				</div>
			</div>
		</div>



	</div>

