<?php //include config
require_once('../includes/config.php');

//if not logged in redirect to login page
if(!$user->is_logged_in()){ header('Location: login.php'); }
?>
<?php include('menu.php');?>

<?php

	//if form has been submitted process it
if(isset($_POST['submit'])){

	$_POST = array_map( 'stripslashes', $_POST );

		//collect form data
	extract($_POST);

		//very basic validation
	if( strlen($firstName) <= 3){
		$error[] = 'Too short first name.';
	}
	if( strlen($middleName) <= 3){
		$error[] = 'Too short middle name.';
	}
	if( strlen($lastName) <= 3){
		$error[] = 'Too short last name.';
	}
	if( strlen($diplomaTheme) <= 3){
		$error[] = 'Too short diploma theme.';
	}
	if( strlen($diplomaDesc) <= 5){
		$error[] = 'Diploma Description is too short!';
	}
	if( strlen($diploma_desc_short) <= 3 && strlen($diploma_desc_short) >= 255){
		$error[] = 'Diploma short description from 15 to 255 symbols!';
	}
	if( strlen($lead_by_teacher) <= 5){
		$error[] = 'Who is your Teacher!?';
	}
	if(!isset($error)){

		try {


			$userSlug = slug(uniqid($diplomaTheme, true));


				//insert into database
			$stmt = $db->prepare('INSERT INTO blog_posts_seo ( postID, firstName,middleName,lastName,dipSubDate,diplomaTheme,diplomaDesc,studentDiploma, userSlug,diploma_desc_short, student_avatar,lead_by_teacher) VALUES (postID, :firstName, :middleName, :lastName, :dipSubDate, :diplomaTheme, :diplomaDesc, :studentDiploma, :userSlug, :diploma_desc_short, :student_avatar, :lead_by_teacher)') ;
			$stmt->execute(array(
				':dipSubDate' => date('Y-m-d'),
				':firstName' => $firstName,
				':middleName' => $middleName,
				':lastName' => $lastName,
				':diplomaTheme' => $diplomaTheme,
				':diplomaDesc' => $diplomaDesc,
				':studentDiploma' => $studentDiploma,
				':userSlug' => $userSlug,
				':diploma_desc_short' => $diploma_desc_short,
				':student_avatar' => $student_avatar,
				':lead_by_teacher' => $lead_by_teacher

				));
			$postID = $db->lastInsertId();


				//redirect to index page
			header('Location: index.php?action=added');
			exit;

		} catch(PDOException $e) {
			echo $e->getMessage();
		}

	}

}

	//check for any errors
if(isset($error)){
	foreach($error as $error){
		echo '<p class="error">'.$error.'</p>';
	}
}
?>

<div class="container">
	<form action='' method='post'>
		<fieldset class="form-group">
			<label for="exampleInputEmail1">First Name</label>
			<input type="text" class="form-control" id="exampleInputEmail1" placeholder="Type students first name" name='firstName' maxlength="125" minlength="3">
		</fieldset>
		<fieldset class="form-group">
			<label for="exampleInputPassword1">Middle Name</label>
			<input type="text" class="form-control" id="exampleInputPassword1" placeholder="Type students middle name" name='middleName' maxlength="125" minlength="3">
		</fieldset>
		<fieldset class="form-group">
			<label for="exampleInputPassword1">Last Name</label>
			<input type="text" class="form-control" id="exampleInputPassword1" placeholder="Type students last name" name='lastName' maxlength="125" minlength="3">
		</fieldset>
		<fieldset class="form-group">
			<label for="exampleInputPassword1">Lead By Teacher</label>
			<input type="text" class="form-control" id="exampleInputPassword1" placeholder="Type Teacher Name" name='lead_by_teacher' maxlength="125" minlength="3">
		</fieldset>
		<fieldset class="form-group">
			<label for="exampleInputPassword1">User Avatar URL</label>
			<input type="text" class="form-control" id="exampleInputPassword1" name='student_avatar' maxlength="222" minlength="3">
		</fieldset>
		<fieldset class="form-group">
			<label for="exampleInputPassword1">Diploma Theme</label>
			<input type="text" class="form-control"  placeholder="Type students diploma theme" name='diplomaTheme' maxlength="125" minlength="3">
		</fieldset>
		<fieldset class="form-group">
			<label for="exampleTextarea">Short Diploma Description</label>
			<textarea class="form-control" id="exampleTextarea" rows="3" name='diploma_desc_short' placeholder="Type some additional information about students diploma" minlength="3" maxlength="333"></textarea>
		</fieldset>
		<fieldset class="form-group">
			<label for="exampleTextarea">Diploma Description</label>
			<textarea class="form-control" id="exampleTextarea" rows="3" name='diplomaDesc' placeholder="Type some main information about students diploma" minlength="3" maxlength="5555"></textarea>
		</fieldset>

			<!-- <fieldset class="form-group">
			<label for="exampleInputFile">Student Avatar </label>
			<input type="file" class="form-control-file" id="exampleInputFile" name='>
		</fieldset>
		<fieldset class="form-group">
			<label for="exampleInputFile">Diploma File</label>
			<input type="file" class="form-control-file" id="exampleInputFile" name='studentDiploma'>
		</fieldset> -->
	<!-- <form action="../upload.php" method="post" enctype="multipart/form-data">
		Select image to upload:
		<input type="file" name="fileToUpload" id="fileToUpload">
		<input type="submit" value="Upload" name="submit">
	</form> -->
	<fieldset class="form-group">
		<label for="exampleInputFile">Diploma File</label>
		<input type="file" name="fileToUpload" id="fileToUpload">
	</fieldset>

	<button type="submit" value='Submit' name='submit' class="btn btn-primary">Submit</button>
</form>

</div>

<?php include('../upload.php');?>
