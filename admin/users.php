<?php
//include config
require_once('../includes/config.php');

//if not logged in redirect to login page
if(!$user->is_logged_in()){ header('Location: login.php'); }

//show message from add / edit page
if(isset($_GET['deluser'])){

	//if user id is 1 ignore
	if($_GET['deluser'] !='1'){

		$stmt = $db->prepare('DELETE FROM blog_members WHERE memberID = :memberID') ;
		$stmt->execute(array(':memberID' => $_GET['deluser']));

		header('Location: users.php?action=deleted');
		exit;

	}
}

?>

<?php include('menu.php');?>

<div class="container">

	<?php
	//show message from add / edit page
	if(isset($_GET['action'])){
		echo '<h3>User '.$_GET['action'].'.</h3>';
	}
	?>

	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row">
				<div class="col-md-2"><a href='add-user.php'>
					<div class="btn btn-success">Add Administrator</div>
				</a></div>
				<div class="col-md-10">
					<form  role="search">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon2"><i class="fa fa-blind " aria-hidden="true"></i></span>
								<input type="text" class="form-control" placeholder="Search">
								<span class="input-group-addon" id="basic-addon2"><i class="fa fa-search " aria-hidden="true"></i> Search</span>
							</div>
						</div>
					</form></div>

				</div>

			</div>
			<div class="table-responsive">
				<table class="table table-bordered  table-hover">
					<thead calss="table-sm">
						<tr class="info">
							<th>Username</th>
							<th>Email</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>

						<?php
						try {

							$stmt = $db->query('SELECT memberID, username, email FROM blog_members ORDER BY username');
							while($row = $stmt->fetch()){

								echo '<tr>';
								echo '<td>'.$row['username'].'</td>';
								echo '<td>'.$row['email'].'</td>';
								?>
								<td>
									<div class="btn-group ">
										<a href="edit-user.php?id=<?php echo $row['memberID'];?>">
											<button class="btn btn-warning btn-responsive text_black" title="View">
												<i class="fa fa-pencil"></i><span class="hidden-xs"> Edit</span>
											</button>
										</a>
										<?php if($row['memberID'] != 1){?>
										<a href="javascript:deluser('<?php echo $row['memberID'];?>','<?php echo $row['username'];?>')">
											<button class="btn btn-danger btn-responsive text_black" title="Delete">
												<i class="fa fa-times"></i><span class="hidden-xs"> Delete</span>
											</button>
										</a>


									</div>
								</td>


								<?php
								echo '</tr>';

							}
						}
					} catch(PDOException $e) {
						echo $e->getMessage();
					}
					?>
				</tbody>
			</table>
		</div>
	</div>

</div>
</div>
</body>
</html>
